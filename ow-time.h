/*

    cc65 1-Wire Library
    Copyright (C) 2003-2007 Scott Alfter (scott@alfter.us)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 2.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

#ifndef OW_TIME_H
#define OW_TIME_H

#include "ow.h"
#include <time.h>

// 1-Wire real-time clock functions

time_t ow_time_read(unsigned char* devid);
void ow_time_set(unsigned char* devid, time_t newtime);
unsigned char ow_time_idcheck(unsigned char* devid);

#endif // OW_TIME_H

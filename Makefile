all: CFRIDGE

CFRIDGE: *.c *.h *.s
	cl65 -C apple2-tgi.cfg -t apple2enh -o CFRIDGE *.c *.s 

clean:
	-rm *.o CFRIDGE

tarball:
	cd .. && tar czvf a2bfc-1.0.tar.gz a2bfc/*.[csh] a2bfc/Makefile a2bfc/apple2-tgi.cfg a2bfc/cfridge.shk a2bfc/interface.txt a2bfc/README a2bfc/COPYING 

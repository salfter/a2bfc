/*

    Apple II Beer Fridge Controller
    Copyright (C) 2007 Scott Alfter (scott@alfter.us)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 2.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

#ifndef _MAIN_H
#define _MAIN_H

typedef enum {COMP_OFF, COMP_ON} COMP_STATUS;

int main (void);
int readYN();
int readnum(int defval, unsigned char timeout);
void backup();
void relayctrl(unsigned char on);
void drawgraph(unsigned char newsample);
int yscale(int y);

#endif // _MAIN_H

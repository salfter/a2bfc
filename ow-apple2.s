;    cc65 1-Wire Library
;    Copyright (C) 2003-2007 Scott Alfter (scott@alfter.us)
;
;    This program is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, version 2.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program; if not, write to the Free Software
;    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

; Apple II-specific 1-Wire routines

	.export _ow_send1_impl
	.export _ow_send0_impl
	.export _ow_read_impl
	.export _ow_reset_impl
	.export _ow_wait_impl
	.export _ow_mswait_impl

; timing loops assume standard ~1 MHz speed for unaccelerated
; 8-bit Apple IIs

AN0	:=	$C058
PB2	:=	$C063

	.code

; send a 1 bit to the 1-Wire bus

_ow_send1_impl:
	sei
	lda	AN0+1	;pull bus low for 6 us
	nop		;2 us to execute
	lda	AN0	;+4 us to execute=6 us
	ldy	#15	;need 64 us delay now...2 us here
:	dey		;2 us
	bne	:-	;2 us
	cli		;2+15*4+2=64 us
	rts

; send a 0 bit to the 1-Wire bus

_ow_send0_impl:
	sei
	lda	AN0+1	;pull bus low for 60 us
	ldy	#13	;2 us
:	dey		;2 us
	bne	:-	;2 us
	nop		;2 us
	lda	AN0	;2 us...2+13*4+2+4=60 us
	ldy	#2	;2 us...now wait for 10 us
:	dey		;2 us
	bne	:-	;2 us...2+2*4=10 us
	cli
	rts

; read a bit from the bus

_ow_read_impl:
	sei
	lda	AN0+1	;pull bus low for 6 us
	nop		;2 us
	lda	AN0	;+4 us=6 us
	nop		;wait 10 us before read
	nop
	nop
	lda	PB2	;2+2+2+4-10
	bmi	:+	;2 us
	clc		;2 us...0 bit read, so clear carry for now
	bcc	:++	;2 us
:	sec		;2 us...1 bit read, so set carry for now
	bcs	:+	;2 us (do this so either path takes 6 us)
:	ldy	#11	;2 us...need 48 us delay before exiting
:	dey
	bne	:-
	cli		;2+11*4+2=48 us
	rts		;leave result in carry

; reset 1-Wire bus; return 0 if devices are present, 1 if not

_ow_reset_impl:
	sei
	lda	AN0+1	;pull bus low for 480 us
	ldy	#47	;2 us
:	nop		;2 us
	nop		;2 us
	nop		;2 us
	dey		;2 us
	bne	:-	;2 us...each iteration takes 10 us
	nop
	nop
	lda	AN0	;2+47*10+2+2+4=480 us
	ldy	#7	;bus is now released, so wait 70 us
:	nop
	nop
	nop
	dey
	bne	:-
	lda	PB2	;see if a device is present
	bmi	:+	;copy N bit to C bit
	clc
	bcc	:++
:	sec
	bcs	:+
:	ldy	#40	;above took 10 us, so wait another 400 us before exiting
:	nop
	nop
	nop
	dey
	bne	:-
	cli
	rts

; wait for an active device to let go of the bus

_ow_wait_impl:
:	bit	PB2
	bpl	:-
	rts

; wait 1 ms
_ow_mswait_impl:
	sei
	ldy	#100	;100*10=1000 us=1 ms
:	nop
	nop
	nop
	dey
	bne	:-
	cli
	rts

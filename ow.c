/*

    cc65 1-Wire Library
    Copyright (C) 2003-2007 Scott Alfter (scott@alfter.us)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 2.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

#include "ow.h"

unsigned char ow_crc_lookup[256]=
{
  0, 94, 188, 226, 97, 63, 221, 131, 194, 156, 126, 32, 163, 253, 31, 65,
  157, 195, 33, 127, 252, 162, 64, 30, 95, 1, 227, 189, 62, 96, 130, 220,
  35, 125, 159, 193, 66, 28, 254, 160, 225, 191, 93, 3, 128, 222, 60, 98,
  190, 224, 2, 92, 223, 129, 99, 61, 124, 34, 192, 158, 29, 67, 161, 255,
  70, 24, 250, 164, 39, 121, 155, 197, 132, 218, 56, 102, 229, 187, 89, 7,
  219, 133, 103, 57, 186, 228, 6, 88, 25, 71, 165, 251, 120, 38, 196, 154,
  101, 59, 217, 135, 4, 90, 184, 230, 167, 249, 27, 69, 198, 152, 122, 36,
  248, 166, 68, 26, 153, 199, 37, 123, 58, 100, 134, 216, 91, 5, 231, 185,
  140, 210, 48, 110, 237, 179, 81, 15, 78, 16, 242, 172, 47, 113, 147, 205,
  17, 79, 173, 243, 112, 46, 204, 146, 211, 141, 111, 49, 178, 236, 14, 80,
  175, 241, 19, 77, 206, 144, 114, 44, 109, 51, 209, 143, 12, 82, 176, 238,
  50, 108, 142, 208, 83, 13, 239, 177, 240, 174, 76, 18, 145, 207, 45, 115,
  202, 148, 118, 40, 171, 245, 23, 73, 8, 86, 180, 234, 105, 55, 213, 139,
  87, 9, 235, 181, 54, 104, 138, 212, 149, 203, 41, 119, 244, 170, 72, 22,
  233, 183, 85, 11, 136, 214, 52, 106, 43, 117, 151, 201, 74, 20, 246, 168,
  116, 42, 200, 150, 21, 75, 169, 247, 182, 232, 10, 84, 215, 137, 107, 53
};

void ow_selectdevice(unsigned char* devid)
{
  ow_reset();
  ow_writebyte(85);
  ow_writebyte(devid[0]);
  ow_writebyte(devid[1]);
  ow_writebyte(devid[2]);
  ow_writebyte(devid[3]);
  ow_writebyte(devid[4]);
  ow_writebyte(devid[5]);
  ow_writebyte(devid[6]);
  ow_writebyte(devid[7]);
  return;
}

unsigned char ow_crc(unsigned char* data, unsigned char len)
{
  unsigned char crc=0;
  unsigned char i;
  
  for (i=0; i<len; i++)
    crc^=ow_crc_lookup[data[i]];
  return crc;
}

unsigned char ow_enumeratedevices(unsigned char* devids, unsigned char maxdevs)
{
  unsigned char bits[64];
  unsigned char last_discrepancy=0;
  unsigned char done=0;
  unsigned char devnum=0;
  unsigned char bit_index;
  unsigned char discrepancy_marker;
  unsigned char bit_1, bit_2;
  unsigned char i, j, offset;
  
  while (!done && devnum<maxdevs)
  {
    if (ow_reset())
    {
      last_discrepancy=0;
      break;
    }
    bit_index=1;
    discrepancy_marker=0;
    ow_writebyte(240); // ROM search
    for (bit_index=1; bit_index<65; bit_index++)
    {
      bit_1=ow_readbit();
      bit_2=ow_readbit();
      if (bit_1 & bit_2)
      {
        last_discrepancy=0;
        break;
      }
      if (!(bit_1 | bit_2))
      {
        if (bit_index==last_discrepancy)
          bits[bit_index-1]=1;
        else
        if (bit_index>last_discrepancy)
        {
          bits[bit_index-1]=0;
          discrepancy_marker=bit_index;
        }
        else
        if (bits[bit_index-1]==0)
          discrepancy_marker=bit_index;
      }
      else
        bits[bit_index-1]=bit_1;
      ow_writebit(bits[bit_index-1]);
    }
    last_discrepancy=discrepancy_marker;
    if (last_discrepancy==0)
      done=1;

    offset=devnum<<3;
    for (i=0; i<8; i++)
    {
      devids[offset+i]=0;
      for (j=0; j<8; j++)
        devids[offset+i]=(devids[offset+i]>>1)+(bits[(i<<3)+j]<<7);
    }
    devnum++;
  }
  return devnum;
}


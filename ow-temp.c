/*

    cc65 1-Wire Library
    Copyright (C) 2003-2007 Scott Alfter (scott@alfter.us)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 2.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

#include "ow-temp.h"
#include <errno.h>

short ow_temp_read(unsigned char* devid, unsigned char format)
{
  unsigned char data[8];
  static int lasttemp=-32767;
  int newtemp;
  int rawtemp;
  unsigned char i;

  if (!ow_temp_idcheck(devid))
  {
    errno=EINVAL;
    return -1;
  }
  ow_selectdevice(devid);
  ow_writebyte(68); // ask for a temperature reading
  ow_wait(); // wait for operation to complete
  ow_selectdevice(devid);
  ow_writebyte(190); // retrieve the reading
  for (i=0; i<8; i++)
    data[i]=ow_readbyte();
  
  rawtemp=((int)data[0]+((int)data[1]<<8));
  errno=0;
  switch (format)
  {
  case OW_TEMP_FAHRENHEIT:
    newtemp=(rawtemp*9+8)/80+32;
    break;
  case OW_TEMP_CELSIUS:
    newtemp=(rawtemp+8)>>4;
    break;
  case OW_TEMP_RAW:
  default:
    return (short)rawtemp;
  }
  /* 17 Aug 08: filter out garbage readings */
  if ((newtemp-lasttemp>3 || newtemp-lasttemp<-3) && lasttemp!=-32767)
    return (short)lasttemp;
  else
  {
    lasttemp=newtemp;
    return (short)newtemp;
  }
}

unsigned char ow_temp_idcheck(unsigned char* devid)
{
  return (devid[0]==40)?1:0;
}

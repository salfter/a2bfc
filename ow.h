/*

    cc65 1-Wire Library
    Copyright (C) 2003-2007 Scott Alfter (scott@alfter.us)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 2.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

#ifndef OW_H
#define OW_H

// common assembly-language routines in ow-common.s

void __fastcall__ ow_writebit(unsigned char data);
void __fastcall__ ow_writebyte(unsigned char data);
unsigned char __fastcall__ ow_readbit(void);
unsigned char __fastcall__ ow_readbyte(void);
unsigned char __fastcall__ ow_reset(void);
void __fastcall__ ow_wait(void);
void __fastcall__ ow_mswait(void);

// common C routines in ow.c

void ow_selectdevice(unsigned char* devid);
unsigned char ow_crc(unsigned char* data, unsigned char len);
unsigned char ow_enumeratedevices(unsigned char* devids, unsigned char maxdevs);

#endif // OW_H

;    cc65 1-Wire Library
;    Copyright (C) 2003-2007 Scott Alfter (scott@alfter.us)
;
;    This program is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, version 2.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program; if not, write to the Free Software
;    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

; system-independent 1-Wire routines

	.export _ow_writebit
	.export _ow_writebyte
	.export _ow_readbit
	.export	_ow_readbyte
	.export _ow_reset
	.export _ow_wait
	.export _ow_mswait

; provided by system-specific routines at link time

	.import	_ow_send1_impl
	.import	_ow_send0_impl
	.import	_ow_read_impl
	.import	_ow_reset_impl
	.import _ow_wait_impl
	.import _ow_mswait_impl

	.bss

owtmp:	.res	1

	.code

; some system-specific routines can be passed through

_ow_send1	:=	_ow_send1_impl
_ow_send0	:=	_ow_send0_impl
_ow_wait	:=	_ow_wait_impl
_ow_mswait	:=	_ow_mswait_impl

; reset bus: return 0 if 1-Wire devices present, 1 otherwise

_ow_reset:
	jsr	_ow_reset_impl
	ldx	#0
	txa
	adc	#0
	rts

; read a bit from the 1-Wire bus

_ow_readbit:
	jsr	_ow_read_impl
	ldx	#0
	txa
	adc	#0
	rts

; write a bit to the 1-Wire bus

_ow_writebit:
	ror		;shift out of LSB
	bcs	:+
	jmp	_ow_send0
:	jmp	_ow_send1

; write a byte to the 1-Wire bus

_ow_writebyte:
	sta	owtmp
	ldx	#8
:	ror	owtmp	;LSB is written first
	bcs	:+
	jsr	_ow_send0
	clc
	bcc	:++
:	jsr	_ow_send1
:	dex
	bne	:---
	rts

; read a byte from the 1-Wire bus

_ow_readbyte:
	ldx	#8
:	jsr	_ow_read_impl
	ror	owtmp
	dex
	bne	:-
	lda	owtmp
	ldx	#0
	rts

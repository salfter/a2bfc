This package serves two purposes: it contains routines for working with
Dallas Semiconductor's 1-Wire and iButton devices on 6502-based systems (the
Apple II, specifically, though it should be easily extended to other
platforms), and it contains an example program that uses these routines to
implement a smart temperature controller.  I use it with an Apple IIe and a
small adapter board that plugs into the joystick port.  It keeps the
temperature within 2 degrees of the setpoint, keeps the compressor from
short-cycling (it's currently set for a 5-minute minimum delay from off to
on), slowly ramps temperatures up/down as needed, and charts the temperature
for the last ~4.5 hours.  I use it to control the fermentation temperature
for my homebrew. :-)

I originally wrote this in a mix of BASIC and assembly language, using
native tools on the Apple II.  Rewriting the entire package with cc65 was an
opportunity to get up to speed on cc65, as I've worked out a design for a
6502-based controller that'll take the Apple II's place and I wanted
something easier to code than having to write everything in assembly
language.  The next step is to adapt cc65 to my custom architecture.

To use just the 1-Wire routines in your own programs, you just need the
ow*.[chs] files added to your project.  If you're using something other than
an Apple II, you'll need to replace ow-apple2.s with something appropriate
for your system (you would also need to cobble together a 1-Wire interface
of some sort, whether it's an RS-232-to-1-Wire adapter on a serial port or a
bit-banging interface of your own design.

ow-apple2.s is probably the best-documented of the files.  All of the
functions within would need to be reimplemented for another system.  Once
you have that done, the other functions should work.

main.[ch] serves as an example.  It's not particularly well-documented, but
in addition to using the 1-Wire temperature and time routines, it also
includes input routines that use the clock to time out after a period of
inactivity.  It expects to have a DS18B20 temperature sensor and a DS2417
real-time clock attached to the 1-Wire bus.  On the 16-pin joystick port,
AN0 writes to the bus (on pulls the bus low; off lets it float high), PB2
reads the current state of the 1-Wire bus, and AN1 turns a relay on and off.

If you're using a IIGS connected to a file server (either AppleShare,
netatalk, or the built-in file sharing on most pre-OSX Macs), the 1-Wire
routines will not work properly while the computer is connected.  If you
kick down to normal speed (1 MHz) and press Ctrl-Reset, the computer will
disconnect from the server and the 1-Wire routines will work properly. 
(This caused me no end of problems in the early phases of development until
I figured this out.  Masking interrupts in the timing-critical routines made
no difference.)

File manifest:

apple2-tgi.cfg
  config file for build
Makefile
  makefile (duh)
cfridge.shk
  ready-to-run Apple II binaries
COPYING
  GPLv2
interface.txt
  a description of the 1-Wire interface for the Apple II joystick port
main.c
main.h
  main program source & header files
ow-apple2.s
  Apple II-specific 1-Wire primitives
ow-common.s
  system-independent 1-Wire primitives
ow.c
ow.h
  higher-level 1-Wire functions (all 1-Wire programs include ow.h)
ow-temp.c
ow-temp.h
  functions for the DS18B20 teperature sensor
ow-time.c
ow-time.h
  functions for the DS2417 real-time clock
README
  this file (duh, again)


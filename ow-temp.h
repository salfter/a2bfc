/*

    cc65 1-Wire Library
    Copyright (C) 2003-2007 Scott Alfter (scott@alfter.us)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 2.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

#ifndef OW_TEMP_H
#define OW_TEMP_H

#include "ow.h"

// 1-Wire temperature sensor functions
// (works with DS18B20; unknown if it'll work with others)

short ow_temp_read(unsigned char* devid, unsigned char format);
unsigned char ow_temp_idcheck(unsigned char* devid);

// 1-Wire temperature sensor constants

#define OW_TEMP_RAW 0
#define OW_TEMP_FAHRENHEIT 1
#define OW_TEMP_CELSIUS 2

#endif // OW_TEMP_H

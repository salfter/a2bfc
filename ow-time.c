/*

    cc65 1-Wire Library
    Copyright (C) 2003-2007 Scott Alfter (scott@alfter.us)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 2.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

#include "ow-time.h"
#include <errno.h>

time_t ow_time_read(unsigned char* devid)
{
  time_t t;
  unsigned char i;
  
  if (!ow_time_idcheck(devid))
  {
    errno=EINVAL;
    return -1;
  }
  ow_selectdevice(devid);
  ow_writebyte(102); // read clock
  ow_readbyte(); // skip first byte returned
  for (i=0; i<4; i++) // read time
    ((unsigned char*)&t)[i]=ow_readbyte();
  ow_reset();
  errno=0;
  return t;    
}

void ow_time_set(unsigned char* devid, time_t newtime)
{
  unsigned char i;
  
  if (!ow_time_idcheck(devid))
  {
    errno=EINVAL;
    return;
  }
  ow_selectdevice(devid);
  ow_writebyte(153); // set clock
  ow_writebyte(12); // device control: start oscillator
  for (i=0; i<4; i++) // write time
    ow_writebyte(((unsigned char*)&newtime)[i]);
  ow_reset();
  errno=0;
  return;
}

unsigned char ow_time_idcheck(unsigned char* devid)
{
  return (devid[0]==39)?1:0;
}
